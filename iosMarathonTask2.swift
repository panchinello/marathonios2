print("Создать массив элементов 'кол-во дней в месяцах' содержащих количество дней в соответствующем месяце")
let numberOfDaysInMounth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

print("Создать массив элементов 'название месяцов' содержащий названия месяцев")
let namesOfMonths = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]

print("Используя цикл for и массив 'кол-во дней в месяцах' выведите количество дней в каждом месяце (без имен месяцев)")
for days in numberOfDaysInMounth {
	print(days)
}

print("Используйте еще один массив с именами месяцев, чтобы вывести название месяца + количество дней")
for i in 0..<namesOfMonths.count {
	print("\(namesOfMonths[i]) \(numberOfDaysInMounth[i])")
}

print("Сделайте тоже самое, но используя массив tuples (кортежей) с параметрами (имя месяца, кол-во дней)")
let monthsTuple: [(name: String, days: Int)] = Array(zip(namesOfMonths, numberOfDaysInMounth))
for month in monthsTuple {
    print("\(month.name) \(month.days)")
}

print("Сделайте тоже самое, только выводите дни в обратном порядке (порядок в массиве не менять)")
for (index, month) in monthsTuple.enumerated() {
  print("\(month.name) \(numberOfDaysInMounth[numberOfDaysInMounth.count - 1 - index])")
}

print("Для произвольно выбранной даты (месяц и день) посчитайте количество дней до этой даты от начала года")
let month = Int.random(in: 0..<namesOfMonths.count)
let day = Int.random(in: 1...numberOfDaysInMounth[month])
var sum = 0

for i in 0..<month {
    sum += numberOfDaysInMounth[i]
}
sum += day - 1
print("Дата: \(namesOfMonths[month]), \(day). Дней с начала года: \(sum)")